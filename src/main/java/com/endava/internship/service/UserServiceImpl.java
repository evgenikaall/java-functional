package com.endava.internship.service;

import com.endava.internship.domain.Privilege;
import com.endava.internship.domain.User;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toList;

public class UserServiceImpl implements UserService {

    @Override
    public List<String> getFirstNamesReverseSorted(List<User> users) {
        return users.stream()
                .sorted(comparing(User::getFirstName).reversed())
                .map(User::getFirstName)
                .collect(toList());
    }

    @Override
    public List<User> sortByAgeDescAndNameAsc(final List<User> users) {
        return users.stream()
                .sorted(comparing(User::getAge).reversed().thenComparing(User::getFirstName))
                .collect(toList());
    }

    @Override
    public List<Privilege> getAllDistinctPrivileges(final List<User> users) {
        return users.stream()
                .map(User::getPrivileges)
                .flatMap(Collection::stream)
                .distinct()
                .collect(toList());
    }

    @Override
    public Optional<User> getUpdateUserWithAgeHigherThan(final List<User> users, final int age) {
        return users.stream()
                .filter(user -> user.getAge() > age)
                .findAny();
    }

    @Override
    public Map<Integer, List<User>> groupByCountOfPrivileges(final List<User> users) {
        return users.stream()
                .collect(Collectors.groupingBy(user -> user.getPrivileges().size()));
    }

    @Override
    public double getAverageAgeForUsers(final List<User> users) {
        return users.stream()
                .mapToDouble(User::getAge)
                .average()
                .orElse(-1);
    }

    @Override
    public Optional<String> getMostFrequentLastName(final List<User> users) {
        final Map<String, Long> map = users.stream()
                .collect(Collectors.groupingBy(User::getLastName, Collectors.counting()));

        final Optional<Long> maximum = map.values().stream().max(Long::compareTo);

        final boolean isValidValue = maximum.isPresent() && maximum.get() > 1;

        return isValidValue ? map.entrySet().stream()
                .filter(entry -> entry.getValue().equals(maximum.orElse(-1L)))
                .findAny().map(Map.Entry::getKey) :
                Optional.empty();

    }

    @SafeVarargs
    @Override
    public final List<User> filterBy(final List<User> users, final Predicate<User>... predicates) {
        return users.stream()
                .filter(Arrays.stream(predicates).reduce(predicate -> true, Predicate::and))
                .collect(toList());
    }

    @Override
    public String convertTo(final List<User> users, final String delimiter, final Function<User, String> mapFun) {
        final StringBuilder converted = new StringBuilder(users.stream()
                .map(mapFun)
                .reduce("", (tempRes, lastLame) -> tempRes.concat(lastLame).concat(delimiter)));

        return converted.deleteCharAt(converted.length() - 1).toString();
    }

    @Override
    public Map<Privilege, List<User>> groupByPrivileges(List<User> users) {
        final HashMap<Privilege, List<User>> resultMap = new HashMap<>();

        users.forEach(user -> user.getPrivileges().forEach(privilege -> {
            if (!resultMap.containsKey(privilege)) resultMap.put(privilege, new ArrayList<>());
            resultMap.get(privilege).add(user);
        }));

        return resultMap;
    }

    @Override
    public Map<String, Long> getNumberOfLastNames(final List<User> users) {
        return users.stream()
                .map(User::getLastName)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
    }
}
